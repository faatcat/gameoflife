/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Game(width, height) {
    this.width = width;
    this.height = height;
    this.status = "STOP";
    this.interval = 333;
    this.timer = 0;
    document.getElementById("startBtn").disabled = false;
    document.getElementById("stopBtn").disabled = true;
    //INITIALIZE DOM FIELD CONTAINER
    this.coordinates = Array(width);
    for (var x = 0; x < width; x++) {
        this.coordinates[x] = new Array(height);
        for (var y = 0; y < height; y++) {
            this.coordinates[x][y] = 0;
        }
    }
    //populate DOM
    var parentDiv = document.getElementById("gameDiv");
    for (var y = 0; y < height; y++) {
        var row = document.createElement("div");
        row.setAttribute("class", "row");
        parentDiv.appendChild(row);
        this.coordinates[x] = new Array(height);
        for (var x = 0; x < width; x++) {
            var block = document.createElement("div");
            this.coordinates[x][y] = block;
            block.setAttribute("x", x);
            block.setAttribute("y", y);
            block.setAttribute("class", "block");
            block.setAttribute("cell", "0");
            block.setAttribute("onClick", "game.clickedCell(" + x + "," + y + ")");
            row.appendChild(block);
        }
    }
    //create map
    this.map = new Map(width, height);

    this.step = function () {
        this.map.step();
        this.redraw();
    }

    this.redraw = function () {
        for (var x = 0; x < this.map.width; x++) {
            for (var y = 0; y < this.map.height; y++) {
                if (this.map.hasCell(x, y)) {
                    this.coordinates[x][y].setAttribute("cell", "1");
                } else {
                    this.coordinates[x][y].setAttribute("cell", "0");
                }
            }
        }
    }

    this.clickedCell = function (x, y) {
        if (this.map.hasCell(x, y)) {
            this.map.killCell(x, y);
        } else {
            this.map.birthCell(x, y);
        }
        this.redraw();
    }

    this.changeInterval = function () {
        var input = document.getElementById("gameInterval");
        if (input.value < 0)
            input.value = this.interval;
        else
            this.interval = input.value;

        if (this.status = "START") {
            this.stop();
            this.start();
        }
    }

    this.start = function () {
        if (this.status == "STOP") {
            this.status = "START";
            document.getElementById("startBtn").disabled = true;
            document.getElementById("stopBtn").disabled = false;
            this.timer = setInterval(function () {
                game.step();
            }, this.interval);
        }
    }

    this.stop = function () {
        this.status = "STOP";
        document.getElementById("startBtn").disabled = false;
        document.getElementById("stopBtn").disabled = true;
        clearInterval(this.timer);
    }

    this.destroy = function () {
        var gameDiv = document.getElementById("gameDiv");
        gameDiv.innerHTML = '';
    }

    this.regenerate = function () {
        this.destroy();
        var width = document.getElementById("gameWidth").value;
        var height = document.getElementById("gameHeight").value;

        return new Game(width, height);
    }
}



//MAP class
function Map(width, height) {
    this.width = width;
    this.height = height;
    //Initializes coordinates as a two dimensional array - coordinates[width][height]
    this.coordinates = new Array(width);
    for (var x = 0; x < width; x++) {
        this.coordinates[x] = new Array(height);
        for (var y = 0; y < height; y++) {
            this.coordinates[x][y] = 0;
        }
    }

    this.step = function () {
        var killStack = new Array();
        var birthStack = new Array();
        //Nested for-loop to iterate through entire Map
        for (var x = 0; x < this.width; x++) {
            for (var y = 0; y < this.height; y++) {
                if (this.hasCell(x, y)) { //Coordinate contains a cell
                    var numNeighbours = new Cell(this, x, y).numNeighbours();
                    if (numNeighbours < 2 || numNeighbours > 3) { //Kill conditions
                        //Prepare to kill cell
                        killStack.push(Array(x, y));
                    }
                } else { //Coordinate does not contain a cell
                    var numNeighbours = new Cell(this, x, y).numNeighbours();
                    if (numNeighbours == 3) { //Birth condition
                        //Prepare to birth new cell
                        birthStack.push(Array(x, y));
                    }
                }
            }
        }

        //Time to update map
        while (birthStack.length > 0) {
            //birth new cells
            var point = birthStack.pop();
            this.birthCell(point[0], point[1]);
        }
        while (killStack.length > 0) {
            //kill cells
            var point = killStack.pop();
            this.killCell(point[0], point[1]);
        }
    }

    this.hasCell = function (x, y) {
        return (this.coordinates[x][y] == 1);
    }
    this.birthCell = function (x, y) {
        this.coordinates[x][y] = 1;
    }
    this.killCell = function (x, y) {
        this.coordinates[x][y] = 0;
    }
}

//CELL class
function Cell(map, x, y) {
    this.coordinatesArray = map.coordinates;
    this.mapWidth = map.width;
    this.mapHeight = map.height;
    this.x = x;
    this.y = y;

    this.numNeighbours = function () {
        var c = this.coordinatesArray;
        var mh = this.mapHeight - 1; //reduce 1 to account for index 0
        var mw = this.mapWidth - 1; //reduce 1 to account for index 0
        var x = this.x;
        var y = this.y;
//      c[x-1][y-1]  c[x][y-1]  c[x+1][y-1]
//      c[x-1][y]      CELL     c[x+1][y]
//      c[x-1][y+1]  c[x][y+1]  c[x+1][y+1]
        //Since coordinatesArray is 1 or 0, sum up a 3x3 area centered around the cell.
        //Checks to prevent array index out of bounds
        return ((x > 0 && y > 0 ? c[x - 1][y - 1] : 0) + (y > 0 ? c[x][y - 1] : 0) + (x < mw && y > 0 ? c[x + 1][y - 1] : 0) +
                (x > 0 ? c[x - 1][y] : 0) + 0 + (x < mw ? c[x + 1][y] : 0) +
                (x > 0 && y < mh ? c[x - 1][y + 1] : 0) + (y < mh ? c[x][y + 1] : 0) + (x < mw && y < mh ? c[x + 1][y + 1] : 0));
    }
}
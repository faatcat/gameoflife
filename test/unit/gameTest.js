/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

describe('testMapInit', function () {
    it('test map initialization', function () {
        var map = new Map(3, 3);
        expect(map.coordinates).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
    });
});
describe('testMapAddRemoveHasCell', function () {
    it('test map basic functionality', function () {
        var map = new Map(3, 3);
        map.birthCell(2, 1);
        expect(map.coordinates).toEqual([[0, 0, 0], [0, 0, 0], [0, 1, 0]]);
        expect(map.hasCell(2, 1)).toEqual(true);
        map.killCell(2, 1);
        expect(map.coordinates).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
    });
});
describe('testMapStepKill', function () {
    it('test map step correctly kills cells', function () {
        var map = new Map(3, 3);
        map.birthCell(0, 0);
        map.birthCell(2, 2);
        map.step();
        expect(map.coordinates).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
    });
});
describe('testMapStepSurvive', function () {
    it('test map step correctly retains cells', function () {
        var map = new Map(3, 3);
        map.birthCell(0, 0);
        map.birthCell(1, 0);
        map.birthCell(0, 1);
        map.step();
        expect(map.coordinates).toEqual([[1, 1, 0], [1, 1, 0], [0, 0, 0]]);

        map = new Map(5, 5);
        map.birthCell(1, 1);
        map.birthCell(2, 2);
        map.birthCell(3, 2);
        map.birthCell(4, 1);
        map.birthCell(1, 4);
        map.birthCell(4, 4);
        map.step();
        expect(map.coordinates).toEqual([[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]]);

    });
});




describe('testCellNumNeighbours', function () {
    it('test cell number of neighbours function', function () {
        var map = new Map(3, 3);
        map.birthCell(2, 1);
        var cell = new Cell(map, 2, 1);
        expect(cell.numNeighbours()).toEqual(0);
        map.birthCell(2, 2);
        expect((new Cell(map, 2, 2)).numNeighbours()).toEqual(1);
        map.birthCell(0, 0);
        expect((new Cell(map, 2, 2)).numNeighbours()).toEqual(1);
        expect((new Cell(map, 0, 0)).numNeighbours()).toEqual(0);
    });
});


